import React from 'react';

const Button = (props) => {
    const [check, set] = props.handleClick;
        return (
            <div>
                <button className="btn-ok" style={{backgroundColor: props.bgc}} onClick={()=>{
                    set(!check);
                    props.closeHandler.modalHandler(!props.closeHandler.isOpenModal);
                }}>{props.text}</button>
            </div>
        );
};

export default Button;
