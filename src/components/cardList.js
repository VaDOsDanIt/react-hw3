import React from 'react';
import Card from './card'

const CardList = (props) => {
        const cards = props.cards !== undefined ? props.cards.map(item => {
            if(item !== undefined){
                return(
                    <Card key={item.article} card={item}/>
                )
            }
            else {
                return undefined;
            }
        }): <p>Товаров нет!</p>;
        return (
                <div className="container p-2 row mx-auto d-flex">
                    {cards}
                </div>
        );
};

export default CardList;