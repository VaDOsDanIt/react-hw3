import React, {useEffect, useState} from 'react';
import Modal from "./Modal";

const Card = (props) => {

    const {name, price, urlImg, article, color} = props.card;
    const cardsArr = JSON.parse(localStorage.getItem('cards'));
    const {favorite, cart} = cardsArr[article];
    const [isFavorite, setFavorite] = useState(favorite);
    const [isCart, setCart] = useState(cart);
    const [isOpenModal, modalHandler] = useState(false);

    const firstModal = {
        header: "Вы хотите добавить данный товар в корзину?",
        closeButton: true,
        text: `Товар: ${props.card.name}`,
        handler: {isOpenModal, modalHandler},
        actions: [{
            text: "Отменить",
            handleClick: [isOpenModal, modalHandler],
        },
            {
                text: "Добавить",
                handleClick: [isCart, setCart],
            }
        ]
    }
    const secondModal = {
        header: "Вы хотите удалить данный товар из корзины?",
        closeButton: true,
        text: `Товар: ${props.card.name}`,
        handler: {isOpenModal, modalHandler},
        actions: [{
            text: "Отменить",
            handleClick: [isOpenModal, modalHandler],
        },
            {
                text: "Удалить",
                handleClick: [isCart, setCart],
            }
        ]
    }


    useEffect(() => {
        const tempArr = cardsArr;
        tempArr[article] = {
            favorite: isFavorite,
            cart: isCart,
        };
        localStorage.setItem('cards', JSON.stringify(tempArr));
    }, [isFavorite, isCart, article, cardsArr]);

    const cartButton = isCart ? <button onClick={(e) => {
            modalHandler(!isOpenModal);
        }} className="btn btn-danger">Убрать из корзины</button> :
        <button onClick={(e) => {
            modalHandler(!isOpenModal);
        }} className="btn btn-success">В корзину</button>;

    const favoriteButton = isFavorite ? <button onClick={(e) => {
            setFavorite(!isFavorite);
        }} className="btn btn-danger">Нравится</button> :
        <button onClick={(e) => {
            setFavorite(!isFavorite);
        }} className="btn btn-success">Нравится</button>;

    let modal;
    if (isOpenModal) {
        modal = isCart ? <Modal modal={secondModal}/> : <Modal modal={firstModal}/>;
    }


    return (
        <div className="col-12 col-md-4 col-lg-3 my-1">
            {modal}
            <div className="w-100 h-100 d-flex justify-content-center border rounded overflow-hidden flex-column">
                <div className="d-flex justify-content-center align-items-end p-2" style={{height: "300px"}}>
                    <img style={{objectFit: "cover"}} height="100%" src={urlImg} alt="img"/>
                </div>
                <table className="w-100">
                    <tbody>
                    <tr className="border-bottom">
                        <td className="text-center w-50 p-1">Model:</td>
                        <td className="text-center w-50 p-1">{name}</td>
                    </tr>
                    <tr className="border-bottom">
                        <td className="text-center w-50 p-1">Price:</td>
                        <td className="text-center w-50 p-1">{price}</td>
                    </tr>
                    <tr className="border-bottom">
                        <td className="text-center w-50 p-1">Article:</td>
                        <td className="text-center w-50 p-1">{article}</td>
                    </tr>
                    <tr className="border-bottom">
                        <td className="text-center w-50 p-1">Color:</td>
                        <td className="text-center w-50 p-1">{color}</td>
                    </tr>
                    </tbody>
                    <tfoot>
                    <tr className="p-2">
                        <td className="text-center w-50 p-2">
                            {cartButton}
                        </td>
                        <td className="text-center w-50 p-2">
                            {favoriteButton}
                        </td>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    );
};

export default Card;