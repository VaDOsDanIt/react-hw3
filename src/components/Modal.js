import React from 'react';
import Button from './Button'

const Modal = (props) => {
        const {header, closeButton, text, actions, handler} = props.modal;

        const btns = actions.map(item => <Button closeHandler={handler} handleClick={item.handleClick} text={item.text}/>);
        const clsButton = closeButton && <a onClick={event => {
            handler.modalHandler(!handler.isOpenModal);
        }} href="#" className="btn-closeModal"> </a>;

        return (
            <div>
                <div className='modal-container'>
                    <div className="modal_">
                        <div className="modal-header">
                            <p className="title">{header}</p>
                            {clsButton}
                        </div>
                        <div className="modal-body">
                            <p className="text">
                                {text}
                            </p>
                        </div>
                        <div className="modal-footer">
                            {btns}
                        </div>
                    </div>
                </div>
            </div>
        )
}

export default Modal
