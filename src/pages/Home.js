import React from 'react';
import CardList from '../components/cardList'
import 'bootstrap/dist/css/bootstrap.min.css';
import {useData} from "../ajax";

const App = () => {

    const {data, isLoading} = useData('./items.json');
    const cardList = isLoading !== true ? <CardList cards={data}/> :
        <div className="spinner-border" role="status">
            <span className="sr-only">Loading...</span>
        </div>;
    return (
        <div>
            {cardList}
        </div>
    );
}

export default App;