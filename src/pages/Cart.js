import React from 'react';
import {useData} from "../ajax";
import CardList from "../components/cardList";

const Cart = () => {
    const {data, isLoading} = useData('./items.json');
    const cardsArr = JSON.parse(localStorage.getItem('cards'));
    let countCards = 0;
    let cards = isLoading !== true ?
        data.map(item => {
            const {cart} = cardsArr[item.article];

            if (cart === true) {
                countCards++;
                return item;
            } else {
                return undefined;
            }
            ;

        }) : undefined;
    console.log(cards);
    const cardList = isLoading !== true && countCards > 0 ? <CardList cards={cards}/> : <p>Товаров в корзине нет.</p>;
    return (
        <div>
            {cardList}
        </div>
    );
}

export default Cart;