import React from 'react';
import {
    BrowserRouter as Router,
    Route,
    Link
} from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.min.css';
import Cart from "./Cart";
import Favorite from "./Favorite";
import Home from './Home';


const App = () => {
    return (
        <Router>
            <div>
                <header className="card-header">
                    <Link className="btn btn-secondary mx-1" to="/home">Товары</Link>
                    <Link className="btn btn-secondary mx-1" to="/cart">Корзина</Link>
                    <Link className="btn btn-secondary mx-1" to="/favorite">Понравившиеся</Link>
                </header>
                <Route path='/home' component={Home}/>
                <Route path='/cart' component={Cart}/>
                <Route path='/favorite' component={Favorite}/>
            </div>
        </Router>
    );
}

export default App;