import { useState, useEffect } from 'react';
export const useData = (url) => {
    const [data, updateData] = useState([]);
    const [isLoading, setLoading] = useState(true);

    useEffect(() => {
        async function fetchData() {
            const response = await fetch(url);
            const json = await response.json();
            updateData(json);
            if(JSON.parse(localStorage.getItem('cards')) === null){
                const obj = {};
                json.map(item => {
                    return obj[item.article] = {
                        favorite: false,
                        cart: false,
                    };
                });
                console.log("Новый обьект создан.");
                localStorage.setItem('cards', JSON.stringify(obj));
            }

            setLoading(false);
        }
        fetchData();
    }, [url]);

    return {data, isLoading};
};